VitalPBX Localization
============

This is the public repository for the VitalPBX localization files.

To provide additional localizations and corrections, please submit a pull
request. For further information please contact [support@vitalpbx.org](mailto:support@vitalpbx.org).

There are two ways of localizing VitalPBX. The preferred method is to use Git as it allows you be notified of future changes and updates. It has multiple tutorials for Mac, Windows and Linux on setting up and working with GIT as well as [desktop clients](https://www.sourcetreeapp.com/):

1. [Create a Bitbucket account](https://bitbucket.org/account/signup/) (if you don't have one already)
2. [Fork the VitalPBX localization repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
3. [Clone your forked repository to your desktop](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
4. [Create and commit changes](https://confluence.atlassian.com/bitbucketserver/commit-and-push-changes-to-bitbucket-server-776798246.html)
5. [Issue a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request)

Alternatively, you can make changes manually and send them by email:

1. Download the latest version ([click here](https://bitbucket.org/SalamanderDevs/vitalpbx-localization/get/78c700629cc4.zip))
2. Make changes
3. Send the result to [support@vitalpbx.org](mailto:support@vitalpbx.org)

Creating a new localization involves creating a new folder (copy en\_US) and defining grammar rules (copy en\_US.rules). You can then start localizing. The localization files follow a simple symbol = "text" format. Only the text part should be translated.
